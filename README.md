# My Editor

It is an App to edit your songs and play them.

My Editor includes features like 
- trimming songs
- merging songs 
- recording voices
- playing songs from your phone storage and also songs from your external SDcard storage.


1. Trimming let you trim songs from wherever you like. It can trim from your choice of time period. It saves trimmed songs in your internal storage and "My Editor" app has feature to show you the trimmed files and you can listen to them too.

2. Merging let you merge songs, whichever you like. It has some constraints in it like you can merge at least two songs and at most four songs. Just like the trimmer function it also saves your file in internal storage. You can also view it and play inside the app.

3. Recording let you record songs from your phone's mic and save it in internal storage. Every recording is save with different name which is in format "dd.MM.YYYY_HH.mm.ss_AM/PM" to diffrentiate form each. You can view and play it inside the app.

4. The app has a dedicated player which let you play songs stored in your phone memory your SDcard memory, also all the trimmed, merged and recorded files saved in your phone.
