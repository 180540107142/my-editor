package com.sama.musicplayer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sama.musicplayer.Adapter.RecordingsAdapter;
import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;

public class RecordingsActivity extends BaseActivity {

    RecordingsAdapter recordingsAdapter;
    RecyclerView rcvrecordings;
    public static ArrayList<File> songList = new ArrayList<File>();

    File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MyFiles/Recordings");

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        else if(item.getItemId()==R.id.action_recorder){
            Intent intent = new Intent(getApplicationContext(), RecordActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId()==R.id.action_home){
            Intent home = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(home);
            this.finishAffinity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recordings, menu);
        return true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        setupActionBar("Recordings", true);
        songList = getThelist(3);
        inItViewReference();
        setAdapter();
    }

    void inItViewReference(){
        rcvrecordings = findViewById(R.id.rcvrecordings);
    }

    void setAdapter(){
        rcvrecordings.setLayoutManager(new GridLayoutManager(this, 1));
        recordingsAdapter = new RecordingsAdapter(this, songList, new RecordingsAdapter.OnItemClickListener() {
            @Override
            public void Onclick(int position) {
                String recordingname = String.valueOf(position);
                startActivity(new Intent(getApplicationContext(), PlayerActivity.class)
                        .putExtra("songs", songList)
                        .putExtra("songname", recordingname)
                        .putExtra("position", position));
            }
        });
        rcvrecordings.setAdapter(recordingsAdapter);
    }

}
