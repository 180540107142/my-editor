package com.sama.musicplayer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sama.musicplayer.Adapter.MergingAdapter;
import com.sama.musicplayer.R;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class MergingActivity extends BaseActivity{

    ListView listView;
    ExtendedFloatingActionButton btnMERGE;
    ArrayList<File> songs = new ArrayList<>();
    ArrayList<Integer> selected = new ArrayList<>();
    ArrayList<File> selectedsong = new ArrayList<>();

    File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/MyFiles/Merged");

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merging);
        setupActionBar("Merging", true);
        initViewReference();
        getSelectedList();
        mergeList();
        setAdapter();

    }

    void initViewReference(){
        listView = findViewById(R.id.lvMerging);
        btnMERGE = findViewById(R.id.btnMERGE);
    }

    void getSelectedList(){

        Intent s = getIntent();
        selected = (ArrayList<Integer>) s.getSerializableExtra("checkedlist");
        songs = (ArrayList<File>) s.getSerializableExtra("songs");
    }

    void mergeList(){
        for (int i=0; i<songs.size(); i++)
        {
            for (int j=0; j<selected.size(); j++){
                if(i==(int)selected.get(j))
                {
                    selectedsong.add(songs.get(i));
                }
            }
        }

    }

    void setAdapter(){

        MergingAdapter mergingAdapter = new MergingAdapter(this, selectedsong);
        listView.setAdapter(mergingAdapter);

        btnMERGE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertdialog();
            }
        });

    }

    void Alertdialog(){

        CustomDialogClass customDialogClass = new CustomDialogClass(MergingActivity.this);
        customDialogClass.show();

    }

    public String createSongName(){
        String name = "";
        for (int i=0; i<selectedsong.size(); i++){
            name = name + selectedsong.get(i).getName().toString().replace(".mp3", "") + "_";
        }
        name = name + ".mp3";
        return name;
    }

    public String createNewSong(String songName) throws IOException {
        if(!path.exists()){
            path.mkdirs();
        }
        String filepath = path + "/Merge_" + songName;
        File file = new File(filepath);
        try {
            file.createNewFile();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return filepath;
    }

    void mergeSongs(String Sname) {
        try {
            int x;
            String pathNewFile = createNewSong(Sname);
            File newFile = new File(pathNewFile);

            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(newFile, true));
            for (int i = 0; i < selectedsong.size(); i++)
            {
                File file = new File(selectedsong.get(i).getAbsolutePath());
                long noOfBytes = file.length();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                while (noOfBytes-->0){
                    x=bufferedInputStream.read();
                    if(x==-1){
                        bufferedOutputStream.write(0);
                    }
                    else{
                        bufferedOutputStream.write(x);
                    }
                }
                bufferedInputStream.close();
            }
            bufferedOutputStream.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        public EditText editText;
        public Button yes, no;
        public boolean status = true;

        public CustomDialogClass(Activity a) {
            super(a);
            this.c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_dialog);
            editText = findViewById(R.id.edtxt_dia);
            editText.setText(createSongName());
            yes = (Button) findViewById(R.id.btn_yes);
            no = (Button) findViewById(R.id.btn_no);
            yes.setOnClickListener(this);
            no.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_yes:
                    if (!editText.getText().toString().endsWith(".mp3")){
                        String n = editText.getText().toString();
                        editText.setText(n.concat(".mp3"));
                    }
                    mergeSongs(editText.getText().toString());
                    Toast.makeText(getApplicationContext(),"DONE", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    c.finish();
                    break;
                case R.id.btn_no:
                    dismiss();
                    break;
                default:
                    break;
            }
            dismiss();
        }
    }
}
