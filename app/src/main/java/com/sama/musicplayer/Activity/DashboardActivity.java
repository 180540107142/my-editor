package com.sama.musicplayer.Activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.sama.musicplayer.R;

import java.io.File;

public class DashboardActivity extends BaseActivity {

    CardView cvRecorder, cvRecordings, cvFiles, cvTrim, cvMerge;
    ExtendedFloatingActionButton efbtnMusic;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        else if(item.getItemId()==R.id.action_songs){
            Intent songs = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(songs);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        inItViewReference();
        inItViewEvent();
    }

    void inItViewReference(){

        cvFiles = findViewById(R.id.cvFiles);
        cvRecorder = findViewById(R.id.cvRecorder);
        cvMerge = findViewById(R.id.cvMerge);
        cvTrim = findViewById(R.id.cvTrim);
        efbtnMusic = findViewById(R.id.efbtn_music);

    }

    void inItViewEvent(){

        cvTrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TrimmingsActivity.class);
                startActivity(intent);
            }
        });

        cvRecorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RecordActivity.class);
                startActivity(intent);
            }
        });

        cvFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FilesActivity.class);
                startActivity(intent);
            }
        });

        cvMerge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MergeActivity.class);
                startActivity(intent);
            }
        });

        efbtnMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

    }

}
