package com.sama.musicplayer.Activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sama.musicplayer.Adapter.MergeAdapter;
import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;

public class MergeActivity extends BaseActivity{

    ListView listView;
    ArrayList<Integer> checkedlist;
    ExtendedFloatingActionButton btnChecked;
    public static ArrayList<File> songList = new ArrayList<File>();

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge);
        setupActionBar("Merge", true);
        initViewReference();
        songList = getThelist(0);
        displayMergings();
    }

    void initViewReference(){

        listView = findViewById(R.id.lvMerges);
        btnChecked = findViewById(R.id.btnChecked);

    }

    void displayMergings(){

        MergeAdapter mergeAdapter = new MergeAdapter(this, songList);
        listView.setAdapter(mergeAdapter);

        btnChecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkedlist = mergeAdapter.getArrayList();
                if(checkedlist.size()>=2){
                        Intent intent = new Intent(getApplicationContext(), MergingActivity.class);
                        intent.putExtra("checkedlist", checkedlist);
                        intent.putExtra("songs", songList);
                        startActivity(intent);
                }else{
                    Toast.makeText(MergeActivity.this, "2 Songs At least", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

}
