package com.sama.musicplayer.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.sama.musicplayer.R;

public class FilesActivity extends BaseActivity {

    CardView crTrimmed, crMerged, crRecordings;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
        setupActionBar("My Files", true);
        inItViewReference();
        inItViewEvent();
    }

    void inItViewReference(){
        crTrimmed = findViewById(R.id.crTrimmed);
        crMerged = findViewById(R.id.crMerged);
        crRecordings = findViewById(R.id.crRecordings);
    }

    void inItViewEvent(){

        crTrimmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SavedActivity.class);
                intent.putExtra("PARENT_MODE", 1);
                startActivity(intent);
            }
        });

        crMerged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SavedActivity.class);
                intent.putExtra("PARENT_MODE", 2);
                startActivity(intent);
            }
        });

        crRecordings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SavedActivity.class);
                intent.putExtra("PARENT_MODE", 3);
                startActivity(intent);
            }
        });
    }
}
