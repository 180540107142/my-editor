package com.sama.musicplayer.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.service.autofill.SaveRequest;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.sama.musicplayer.InputFilterMinMax;
import com.sama.musicplayer.R;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import it.sephiroth.android.library.rangeseekbar.RangeSeekBar;
import rm.com.audiowave.AudioWaveView;
import rm.com.audiowave.OnProgressListener;

public class TrimmerActivity extends BaseActivity {

    LinearLayout atvTrimmer;

    Button btnplay,btnFtenTRIM,btnRtenTRIM,btnMoneTRIM,btnPoneTRIM;
    ExtendedFloatingActionButton btnTRIM;
    TextView tvsongname,tvplayerTRIM;
    EditText etstartplayerTRIM,etstopplayerTRIM;
    private static long startTRIM=1, endTRIM=100;

    AudioWaveView audioWaveView;
    public boolean running = true;
    public boolean Tcapacity = true;
    RangeSeekBar seekBar;

    String songname, songPath;
    static MediaPlayer mediaPlayer;
    int position;
    ArrayList<File> songs;

    Thread updateAudiowave;

    File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/MyFiles/Trimmed");

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        atvTrimmer.requestFocus();
    }

    @Override
    protected void onDestroy() {
        if (audioWaveView!=null){
            audioWaveView = null;
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        running=false;
        updateAudiowave.interrupt();
        Thread.currentThread().interrupt();
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer=null;
        super.onBackPressed();
    }

    public void Backing(){
        onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimmer);
        setupActionBar("Trimmer", true);

        inItViewReference();
        inItViewEvent();
        startPlayer();
        setLineBarVisualizer();
    }

    public void startPlayer(){

        if(mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }

        Intent i = getIntent();
        Bundle bundle = i.getExtras();

        songs = (ArrayList) bundle.getParcelableArrayList("songs");
        position = bundle.getInt("position", 0);
        tvsongname.setSelected(true);
        Uri uri = Uri.parse(songs.get(position).toString());
        songname = songs.get(position).getName();
        tvsongname.setText(songname);
        songPath = songs.get(position).getAbsolutePath();
        etstopplayerTRIM.setFilters(new InputFilter[]{new InputFilterMinMax(1000)});
        etstartplayerTRIM.setFilters(new InputFilter[]{new InputFilterMinMax(1000)});

        try {
            byte[] abc = convert(songPath);
            audioWaveView.setRawData(abc);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
        mediaPlayer.start();

        etstopplayerTRIM.setText(createTime(mediaPlayer.getDuration()));
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run() {
                if (running){
                    String currentTime = createTime(mediaPlayer.getCurrentPosition());
                    tvplayerTRIM.setText(currentTime);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);

        //next listener
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                btnplay.performClick();
                setLineBarVisualizer();
            }
        });

    }

    public void setLineBarVisualizer(){
        updateAudiowave = new Thread()
        {
            @Override
            public void run() {
                if (running){
                    float totalDuration = mediaPlayer.getDuration()/100;
                    float currentPosition = 0;

                    while (currentPosition<totalDuration && running)
                    {
                        if(mediaPlayer.getCurrentPosition()>endTRIM*totalDuration) {
                            mediaPlayer.pause();
                        }
                        try {
                            sleep(100);
                            if (running){
                                currentPosition = mediaPlayer.getCurrentPosition()/100;
                                float percent = currentPosition*100/totalDuration;
                                audioWaveView.setProgress(percent);
                            }
                        }

                        catch (InterruptedException | IllegalStateException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        updateAudiowave.start();
        audioWaveView.setWaveColor(getColor(R.color.design_default_color_primary_dark));
    }

    public void inItViewReference(){

        btnFtenTRIM = findViewById(R.id.btnFtenTRIM);
        btnRtenTRIM = findViewById(R.id.btnRtenTRIM);
        btnTRIM = findViewById(R.id.btnTRIM);
        btnMoneTRIM = findViewById(R.id.btnMoneTRIM);
        btnPoneTRIM = findViewById(R.id.btnPoneTRIM);

        btnplay = findViewById(R.id.btnplayTRIM);
        tvsongname = findViewById(R.id.tvsongnameplayerTRIM);
        tvplayerTRIM = findViewById(R.id.tvplayerTRIM);
        etstartplayerTRIM = findViewById(R.id.etstartplayerTRIM);
        etstopplayerTRIM = findViewById(R.id.etstopplayerTRIM);

        audioWaveView = findViewById(R.id.audioWaveView);
        seekBar = findViewById(R.id.seekbar);

        atvTrimmer = findViewById(R.id.atvTrimmer);

    }

    public void inItViewEvent(){

        audioWaveView.setOnProgressListener(new OnProgressListener() {
            @Override
            public void onStartTracking(float v) {
                int a = Math.round(audioWaveView.getProgress()) * mediaPlayer.getDuration();
                int b = a/100;
                mediaPlayer.seekTo(b);
            }

            @Override
            public void onStopTracking(float v) {
                int a = Math.round(audioWaveView.getProgress()) * mediaPlayer.getDuration();
                int b = a/100;
                mediaPlayer.seekTo(b);
            }

            @Override
            public void onProgressChanged(float v, boolean b) {
            }
        });

        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying()){
                    btnplay.setBackgroundResource(R.drawable.ic_play);
                    mediaPlayer.pause();
                }
                else {
                    if (mediaPlayer.getCurrentPosition()>=endTRIM*mediaPlayer.getDuration()/100)
                    {
                        mediaPlayer.seekTo((int)startTRIM*mediaPlayer.getDuration()/100);
                    }
                    btnplay.setBackgroundResource(R.drawable.ic_pause);
                    mediaPlayer.start();
                }
            }
        });

        btnFtenTRIM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()+10000);
            }
        });

        btnRtenTRIM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()-10000);
            }
        });

        btnTRIM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialogClass customDialogClass = new CustomDialogClass(TrimmerActivity.this);
                customDialogClass.show();
            }
        });

        btnMoneTRIM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limitEdit(etstartplayerTRIM, 3);
            }
        });

        btnPoneTRIM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limitEdit(etstopplayerTRIM,4);
            }
        });

        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onProgressChanged(RangeSeekBar rangeSeekBar, int start, int end, boolean b) {
                int startTIME = start*mediaPlayer.getDuration()/100;
                int endTIME = end*mediaPlayer.getDuration()/100;
                etstartplayerTRIM.setText(createTime(startTIME));
                etstopplayerTRIM.setText(createTime(endTIME));
                startTRIM = (long)start;
                endTRIM = (long)end;
                mediaPlayer.seekTo(startTIME);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar rangeSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar rangeSeekBar) {
            }
        });

        etstopplayerTRIM.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    limitEdit(etstopplayerTRIM, 2);
                    if (!Tcapacity) {
                        etstopplayerTRIM.setText(createTime(mediaPlayer.getDuration()));
                        Toast.makeText(getApplicationContext(), "Max Time Limit", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        etstartplayerTRIM.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    limitEdit(etstartplayerTRIM, 1);
                    if (!Tcapacity) {
                        etstartplayerTRIM.setText(createTime(0));
                        Toast.makeText(getApplicationContext(), "Min Time Limit", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        atvTrimmer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                atvTrimmer.requestFocus();
                return false;
            }
        });
    }

    String createNewSong(String songName) throws IOException {
        if(!path.exists()){
            path.mkdirs();
        }
        String filepath = path + "/" + songName;
        File file = new File(filepath);
        file.createNewFile();
        return filepath;
    }

    public byte[] convert(String path) throws IOException {

        FileInputStream fis = new FileInputStream(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[fis.available()];
        for (int readNum; (readNum = fis.read(b)) != -1;) {
            bos.write(b, 0, readNum);
        }
        byte[] bytes = bos.toByteArray();
        return bytes;
    }

    public void createSong(long start, long end, String songName){

        File file = new File(songs.get(position).getAbsolutePath());
        long duration = (long)mediaPlayer.getDuration();
        long noOfBytes = file.length();
        long bytesToSkip = start*noOfBytes/100;
        long lastByteNo = end*noOfBytes/100;
        long bytesToRead = lastByteNo-bytesToSkip;

        long bytesTRIM = start*noOfBytes/duration;
        try {
            String pathNewFile = createNewSong(songName);
            BufferedInputStream bufferedInputStream=new BufferedInputStream(new FileInputStream(file));
            bufferedInputStream.skip(bytesToSkip);
            File newFile = new File(pathNewFile);
            BufferedOutputStream bufferedOutputStream=new BufferedOutputStream(new FileOutputStream(newFile));
            while (bytesToRead-->0){
                bufferedOutputStream.write(bufferedInputStream.read());
            }
            bufferedOutputStream.close();
            bufferedInputStream.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String createTime(int duration)
    {
        String time = "";
        int min = duration/1000/60;
        int sec = duration/1000%60;
        if(min<10){
            time+="0";
        }
        time+=min+":";
        if(sec<10){
            time+="0";
        }
        time+=sec;
        return time;
    }

    private float limitEdit(EditText et, int mode){
        int min,sec;
        float time;
        if (et.getText().toString()==null||et.getText().toString().length()==0){
            return 0;
        }
        min = Integer.parseInt(et.getText().toString().substring(0,2));
        sec = Integer.parseInt(et.getText().toString().substring(3));
        time = createMillis(min, sec);
        if (time>mediaPlayer.getDuration()){
            Tcapacity = false;
            return 0;
        }
        if (mode == 0){
            return time*100/mediaPlayer.getDuration();
        }
        else if (mode == 1){
            seekBar.setProgress(Math.round(time*100/mediaPlayer.getDuration())
                    ,Math.round(limitEdit(etstopplayerTRIM, 0)));
        }
        else if(mode == 2){
            seekBar.setProgress(Math.round(limitEdit(etstartplayerTRIM, 0))
                    ,Math.round(time*100/mediaPlayer.getDuration()));
        }
        else if (mode == 3){
            seekBar.setProgress(Math.round(((time-1000)*100)/mediaPlayer.getDuration())
                    ,Math.round(limitEdit(etstopplayerTRIM, 0)));
        }
        else if (mode == 4){
            seekBar.setProgress(Math.round(limitEdit(etstartplayerTRIM, 0))
                    ,Math.round(((time+1000)*100)/mediaPlayer.getDuration()));
        }
        Tcapacity = true;
        return 0;
    }

    public int createMillis(int a, int b){
        int time,min,sec;
        min = a*1000*60;
        sec = b*1000;
        time = min+sec;
        return time;
    }

    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity c;
        public Dialog d;
        public EditText editText;
        public Button yes, no;
        public boolean status = true;

        public CustomDialogClass(Activity a) {
            super(a);
            this.c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_dialog);
            editText = findViewById(R.id.edtxt_dia);
            editText.setText("Trim_" + songname);
            yes = (Button) findViewById(R.id.btn_yes);
            no = (Button) findViewById(R.id.btn_no);
            yes.setOnClickListener(this);
            no.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_yes:
                    if(startTRIM==0){
                        startTRIM=1;
                    }
                    if (!editText.getText().toString().endsWith(".mp3")){
                        String n = editText.getText().toString();
                        editText.setText(n.concat(".mp3"));
                    }
                    createSong(startTRIM, endTRIM, editText.getText().toString());
                    Toast.makeText(getApplicationContext(),"DONE", Toast.LENGTH_SHORT).show();
                    Backing();
                    c.finish();
                    break;
                case R.id.btn_no:
                    dismiss();
                    break;
                default:
                    break;
            }
            dismiss();
        }
    }
}
