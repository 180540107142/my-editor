package com.sama.musicplayer.Activity;

import androidx.annotation.NonNull;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gauravk.audiovisualizer.visualizer.BarVisualizer;
import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;

public class PlayerActivity extends BaseActivity {

    public boolean Prunning = true;

    Button btnplay,btnnext,btnprevious,btnfastforward,btnbackward;
    TextView tvsongname,tvstart,tvstop;
    SeekBar sbplayer;
    BarVisualizer visualizer;
    ImageView songimage;

    String songname;
    static MediaPlayer mediaPlayer;
    int position;
    ArrayList<File> songs;

    Thread updateseekbar;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if(visualizer!= null){
            visualizer.release();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Prunning=false;
        updateseekbar.interrupt();
        Thread.currentThread().interrupt();
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer=null;
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        setupActionBar("Now Playing", true);

        inItViewReference();
        inItViewEvent();
        startPlayer();
        Seekbar();
        startVisualizer();

    }

    public void inItViewReference(){
        btnplay = findViewById(R.id.btnplay);
        btnnext = findViewById(R.id.btnnext);
        btnprevious = findViewById(R.id.btnprevious);
        btnfastforward = findViewById(R.id.btnforward);
        btnbackward = findViewById(R.id.btnbackward);
        tvsongname = findViewById(R.id.tvsongnameplayer);
        tvstart = findViewById(R.id.tvstartplayer);
        tvstop = findViewById(R.id.tvstopplayer);
        sbplayer = findViewById(R.id.sbplayer);
        visualizer = findViewById(R.id.blast);
        songimage = findViewById(R.id.imgvsongplayer);
    }

    public void inItViewEvent(){

        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying()){
                    startanimationPause();
                    btnplay.setBackgroundResource(R.drawable.ic_play);
                    mediaPlayer.pause();
                }
                else {
                    startanimationPlay();
                    btnplay.setBackgroundResource(R.drawable.ic_pause);
                    mediaPlayer.start();
                }
            }
        });

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                mediaPlayer.release();
                position = ((position+1)%songs.size());
                Uri u = Uri.parse(songs.get(position).toString());
                mediaPlayer = MediaPlayer.create(getApplicationContext(), u);
                songname = songs.get(position).getName();
                String endtime = createTime(mediaPlayer.getDuration());
                tvstop.setText(endtime);
                tvsongname.setText(songname);
                mediaPlayer.start();
                btnplay.setBackgroundResource(R.drawable.ic_pause);
                startanimationForward();
                nextSetter();//for completion
                int audiosessionId = mediaPlayer.getAudioSessionId();
                if(audiosessionId!=-1){
                    visualizer.setAudioSessionId(audiosessionId);
                }
            }
        });

        btnprevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                mediaPlayer.release();
                position = ((position-1)<0?(songs.size()-1):(position-1));
                Uri u = Uri.parse(songs.get(position).toString());
                mediaPlayer = MediaPlayer.create(getApplicationContext(), u);
                songname = songs.get(position).getName();
                String endtime = createTime(mediaPlayer.getDuration());
                tvstop.setText(endtime);
                tvsongname.setText(songname);
                mediaPlayer.start();
                btnplay.setBackgroundResource(R.drawable.ic_pause);
                startanimationReverse();
                nextSetter();//for completion
                int audiosessionId = mediaPlayer.getAudioSessionId();
                if(audiosessionId!=-1){
                    visualizer.setAudioSessionId(audiosessionId);
                }
            }
        });

        btnfastforward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()+10000);
                }
            }
        });

        btnbackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()-10000);
                }
            }
        });


        //seekbar click event
        sbplayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
            }
        });

    }

    public void startPlayer(){

        if(mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }

        Intent i = getIntent();
        Bundle bundle = i.getExtras();

        songs = (ArrayList) bundle.getParcelableArrayList("songs");
        position = bundle.getInt("position", 0);
        tvsongname.setSelected(true);
        Uri uri = Uri.parse(songs.get(position).toString());
        songname = songs.get(position).getName();
        tvsongname.setText(songname);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
        mediaPlayer.start();

        String endtime = createTime(mediaPlayer.getDuration());
        tvstop.setText(endtime);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run() {
                if (Prunning){
                    String currentTime = createTime(mediaPlayer.getCurrentPosition());
                    tvstart.setText(currentTime);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);

        //next listener
        nextSetter();

    }

    public void nextSetter(){
        if (Prunning){
            Seekbar();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    btnnext.performClick();
                }
            });
        }
    }

    public void Seekbar(){
        updateseekbar = new Thread()
        {
            @Override
            public void run() {
                if (Prunning){
                    int totalDuration = mediaPlayer.getDuration();
                    int currentPosition = 0;
                    while (currentPosition<totalDuration && Prunning)
                    {
                        try {
                            sleep(100);
                            if (Prunning){
                                currentPosition = mediaPlayer.getCurrentPosition();
                                sbplayer.setProgress(currentPosition);
                            }
                        }
                        catch (InterruptedException | IllegalStateException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        sbplayer.setMax(mediaPlayer.getDuration());
        updateseekbar.start();

        sbplayer.getProgressDrawable().setColorFilter(getResources().getColor(R.color.design_default_color_primary_dark), PorterDuff.Mode.MULTIPLY);
        sbplayer.getThumb().setColorFilter(getResources().getColor(R.color.design_default_color_primary_variant), PorterDuff.Mode.SRC_IN);

    }

    public void startVisualizer(){

        int audiosessionId = mediaPlayer.getAudioSessionId();
        if(audiosessionId!=-1){
            visualizer.setAudioSessionId(audiosessionId);
        }

    }

    public void startanimationForward()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(songimage, "rotation", 0f, 360f);

        animator.setDuration(700);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator);
        animatorSet.start();

    }

    public void startanimationPause()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(songimage, "alpha", 1f, 0.5f);

        animator.setDuration(500);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator);
        animatorSet.start();

    }

    public void startanimationPlay()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(songimage, "alpha", 0.5f, 1f);

        animator.setDuration(500);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator);
        animatorSet.start();

    }

    public void startanimationReverse()
    {
        ObjectAnimator animator = ObjectAnimator.ofFloat(songimage, "rotation", 360f, 0f);

        animator.setDuration(700);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator);
        animatorSet.start();

    }

    public String createTime(int duration)
    {
        String time = "";
        int min = duration/1000/60;
        int sec = duration/1000%60;
        if(min<10){
            time+="0";
        }
        time+=min+":";
        if(sec<10){
            time+="0";
        }
        time+=sec;
        return time;

    }

}
