package com.sama.musicplayer.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends BaseActivity {

    ListView listView;
    String[] items;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar("Songs", true);
        listView = findViewById(R.id.lvsong);
        Permission();

    }

    public void Permission(){

        Dexter.withContext(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                        displaysong();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();

    }

    //Search For Songs
    public ArrayList<File> findsong (File file){
        ArrayList<File> arrayList = new ArrayList<>();
        File[] files = file.listFiles();
        if(files!=null){
            for (File singlefile: files)
            {
                if(singlefile.isDirectory() && !singlefile.isHidden())
                {
                    arrayList.addAll(findsong(singlefile));
                }
                else {
                    if (singlefile.getName().endsWith(".mp3")){
                        arrayList.add(singlefile);
                    }
                }
            }
        }
        return arrayList;
    }

    ArrayList<File> merge(ArrayList<File> a, ArrayList<File> b){
        a.addAll(b);
        return a;
    }

    void displaysong(){

        ArrayList<File> songsEX = findsong(Environment.getStorageDirectory());
        ArrayList<File> songsIN = findsong(Environment.getExternalStorageDirectory());
        final ArrayList<File> songs = merge(songsIN, songsEX);
        Collections.sort(songs);

        items = new String[songs.size()];
        for (int i = 0; i<songs.size(); i++)
        {
            items[i] = songs.get(i).getName().toString().replace(".mp3", "");
        }

        customAdapter customAdapter = new customAdapter();
        listView.setAdapter(customAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getApplicationContext(), PlayerActivity.class)
                        .putExtra("songs", songs)
                        .putExtra("position", position));
            }
        });
    }

    //CUSTOM ADAPTER
    class customAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return items.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = getLayoutInflater().inflate(R.layout.list_song, null);
            TextView textsong = view.findViewById(R.id.tvsongname);
            textsong.setSelected(true);
            textsong.setText(items[position]);
            return view;
        }
    }
}