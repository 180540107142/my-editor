package com.sama.musicplayer.Activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sama.musicplayer.Adapter.TrimmingsAdapter;
import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;

public class TrimmingsActivity extends BaseActivity{

    ListView listView;
    String[] items;
    public static ArrayList<File> songList = new ArrayList<File>();

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimmings);
        setupActionBar("Trim", true);
        listView = findViewById(R.id.lvTrimmings);
        songList = getThelist(0);

        displayTrimmings();
    }

    void displayTrimmings(){

//        items = new String[trimmings.size()];
//        for (int i = 0; i<trimmings.size(); i++)
//        {
//            items[i] = trimmings.get(i).getName().toString().replace(".mp3", "");
//        }
        /*ArrayAdapter<String> songadapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(songadapter);*/

        TrimmingsAdapter trimmingsAdapter = new TrimmingsAdapter(this, songList);
        listView.setAdapter(trimmingsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getApplicationContext(), TrimmerActivity.class)
                        .putExtra("songs", songList)
                        .putExtra("position", position));
            }
        });
    }
}
