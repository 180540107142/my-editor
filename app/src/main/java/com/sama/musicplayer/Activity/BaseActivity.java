package com.sama.musicplayer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sama.musicplayer.Adapter.TrimmingsAdapter;
import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    static int mode;
    public static ArrayList<File> theList = new ArrayList<>();

    public void setupActionBar(String title,boolean isBackArrow){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackArrow);
    }

    public ArrayList<File> getThelist(int mode){
        this.mode = mode;
        permission();
        return theList;
    }

    public void permission(){

        Dexter.withContext(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @RequiresApi(api = Build.VERSION_CODES.R)
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                        theList = displaytheList();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    public ArrayList<File> displaytheList(){

        if(mode==0){
            ArrayList<File> songsEX = findSaved(Environment.getStorageDirectory().getAbsolutePath());
            ArrayList<File> songsIN = findSaved(Environment.getExternalStorageDirectory().getAbsolutePath());
            final ArrayList<File> songList = merge(songsIN, songsEX);
            Collections.sort(songList);
            return songList;
        }

        else if(mode==1)
        {
            final ArrayList<File> savings = findSaved(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MyFiles/Trimmed");

            return savings;
        }
        else if(mode==2)
        {
            final ArrayList<File> savings = findSaved(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MyFiles/Merged");

            return savings;
        }
        else if(mode==3)
        {
            final ArrayList<File> savings = findSaved(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MyFiles/Recordings");

            return savings;
        }
        else {
            return null;
        }
    }

    private ArrayList<File> merge(ArrayList<File> a, ArrayList<File> b){
        a.addAll(b);
        return a;
    }

    private ArrayList<File> findSaved (String path){

        ArrayList<File> arrayList = new ArrayList<>();

        File directory = new File(path);
        File[] files = directory.listFiles();

        if(files!=null){
            for (File singlefile: files)
            {
                if(singlefile.isDirectory() && !singlefile.isHidden())
                {
                    arrayList.addAll(findSaved(singlefile.getAbsolutePath()));
                }
                else {
                    if (singlefile.getName().endsWith(".mp3")){
                        arrayList.add(singlefile);
                    }
                    else if(singlefile.getName().endsWith(".amr") && mode==3){
                        arrayList.add(singlefile);
                    }
                }
            }
        }

        return arrayList;

    }
}
