package com.sama.musicplayer.Activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sama.musicplayer.Adapter.TrimmingsAdapter;
import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;

public class SavedActivity extends BaseActivity {

    public static ArrayList<File> songList = new ArrayList<File>();

    public static final String TAG_ACTIVITY_FROM = "WhichActivity";
    static String activityFrom;
    static int mode;
    ListView listView;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        else if(item.getItemId()==R.id.action_saved){
            switch (mode){
                case 1:
                    Intent i1 = new Intent(getApplicationContext(), TrimmingsActivity.class);
                    startActivity(i1);
                    break;
                case 2:
                    Intent i2 = new Intent(getApplicationContext(), MergeActivity.class);
                    startActivity(i2);
                    break;
                case 3:
                    Intent i3 = new Intent(getApplicationContext(), RecordActivity.class);
                    startActivity(i3);
                    break;
            }
        }
        else if(item.getItemId()==R.id.action_home){
            Intent home = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(home);
            super.onBackPressed();
            this.finishAffinity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (mode){
            case 1:
                getMenuInflater().inflate(R.menu.menu_trim, menu);
                break;
            case 2:
                getMenuInflater().inflate(R.menu.menu_merge, menu);
                break;
            case 3:
                getMenuInflater().inflate(R.menu.menu_recorder, menu);
                break;
        }
        return true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i = getIntent();
        mode = i.getIntExtra("PARENT_MODE", 0);
        setToolbar(mode);
        activityFrom = i.getStringExtra(TAG_ACTIVITY_FROM);
        listView = findViewById(R.id.lvsong);
        songList = getThelist(mode);
        displaySavings();
    }

    void setToolbar(int mode){
        switch (mode){
            case 1:
                setupActionBar("Trimmed", true);
                break;
            case 2:
                setupActionBar("Merged", true);
                break;
            case 3:
                setupActionBar("Recorded", true);
                break;
        }
    }

    void displaySavings(){

        if(mode==1)
        {
            TrimmingsAdapter trimmingsAdapter = new TrimmingsAdapter(this, songList);
            listView.setAdapter(trimmingsAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String songname = (String) listView.getItemAtPosition(position);
                    startActivity(new Intent(getApplicationContext(), PlayerActivity.class)
                            .putExtra("songs", songList)
                            .putExtra("songname", songname)
                            .putExtra("position", position));
                }
            });
        }
        else if(mode==2)
        {
            TrimmingsAdapter trimmingsAdapter = new TrimmingsAdapter(this, songList);
            listView.setAdapter(trimmingsAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String songname = (String) listView.getItemAtPosition(position);
                    startActivity(new Intent(getApplicationContext(), PlayerActivity.class)
                            .putExtra("songs", songList)
                            .putExtra("songname", songname)
                            .putExtra("position", position));
                }
            });
        }
        else if(mode==3)
        {
            TrimmingsAdapter trimmingsAdapter = new TrimmingsAdapter(this, songList);
            listView.setAdapter(trimmingsAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String songname = (String) listView.getItemAtPosition(position);
                    startActivity(new Intent(getApplicationContext(), PlayerActivity.class)
                            .putExtra("songs", songList)
                            .putExtra("songname", songname)
                            .putExtra("position", position));
                }
            });
        }

    }
}
