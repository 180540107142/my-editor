package com.sama.musicplayer.Activity;

import android.Manifest;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Environment;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sama.musicplayer.R;
import com.sama.musicplayer.VisualizerView;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.os.Environment.getExternalStorageDirectory;

public class RecordActivity extends BaseActivity {

    TextView tvrecordstatus;
    ImageView btnrecord;
    Chronometer chronometertimerec;
    VisualizerView blastrecorder;
    String date;

    private Handler handler;

    private static String fileName;
    private MediaRecorder mediarecorder;
    boolean isRecording;

    File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/MyFiles"+"/Recordings");

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Intent home = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(home);
            this.finishAffinity();

        }
        else if(item.getItemId()==R.id.action_saved){
            Intent intent = new Intent(getApplicationContext(), RecordingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_saved, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        if(mediarecorder!= null){
            mediarecorder.release();
            handler.removeCallbacks(updateVisualizer);
        }
        super.onDestroy();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorder);
        setupActionBar("Recorder", true);
        if(!path.exists()){
            path.mkdirs();
        }
        handler = new Handler();
        isRecording = false;
        inItViewReference();
        formater();
        fileName = path + "/recording_" + date + ".amr";
        inItViewEvent();

    }

    public void inItViewReference(){
        tvrecordstatus = findViewById(R.id.tvRecordStatus);
        chronometertimerec = findViewById(R.id.chronometerTimeRec);
        blastrecorder = (VisualizerView) findViewById(R.id.blastRecorder);
        btnrecord = findViewById(R.id.btnRecord);
    }

    void formater(){
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy_hh.mm.ss_a", Locale.getDefault());
        date = format.format(new Date());
    }

    public void inItViewEvent(){

        btnrecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isRecording){
                    try {
                        startRecording();
                        chronometertimerec.setBase(SystemClock.elapsedRealtime());
                        chronometertimerec.start();
                        tvrecordstatus.setText("Recording...");
                        btnrecord.setImageResource(R.drawable.ic_record_start);
                        isRecording = true;
                        handler.post(updateVisualizer);
                    }
                    catch (Exception e){
                        Toast.makeText(getBaseContext(), "Couldn't start", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(isRecording){
                    stopRecording();
                    chronometertimerec.setBase(SystemClock.elapsedRealtime());
                    chronometertimerec.stop();
                    tvrecordstatus.setText("Record");
                    btnrecord.setImageResource(R.drawable.ic_record_stop);
                    isRecording = false;
                }
            }
        });

    }

    Runnable updateVisualizer = new Runnable() {
        @Override
        public void run() {
            if (isRecording) // if already recording
            {
                // get the current amplitude
                int x = mediarecorder.getMaxAmplitude();
                blastrecorder.addAmplitude(x); // update the VisualizeView
                blastrecorder.invalidate(); // refresh the VisualizerView

                // update in 40 millis
                handler.postDelayed(this, 40);
            }
        }
    };

    public void startRecording(){

        mediarecorder = new MediaRecorder();
        mediarecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediarecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        mediarecorder.setOutputFile(fileName);
        mediarecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            mediarecorder.prepare();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        mediarecorder.start();

    }

    public void stopRecording(){

        mediarecorder.stop();
        mediarecorder.release();
        mediarecorder = null;
        Toast.makeText(getBaseContext(), "Recorded", 500).show();

    }

}
