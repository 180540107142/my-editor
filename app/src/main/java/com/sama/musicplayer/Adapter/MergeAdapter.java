package com.sama.musicplayer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.sama.musicplayer.Activity.MergeActivity;
import com.sama.musicplayer.R;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class MergeAdapter extends BaseAdapter {

    Context context;
    ArrayList<File> mergings;
    ArrayList<Integer> checkedlist = new ArrayList<>();

    public MergeAdapter(Context context, ArrayList<File> mergings){
        this.context = context;
        this.mergings = mergings;
    }

    @Override
    public int getCount() {
        return mergings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder viewHolder;
        if (v==null){
            v = LayoutInflater.from(context).inflate(R.layout.merge_item, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) v.getTag();
        }
        if(checkedlist.contains(position)){
            viewHolder.checkBox.setChecked(true);
        }else{
            viewHolder.checkBox.setChecked(false);
        }
        viewHolder.tvName.setText(mergings.get(position).getName());
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isChecked=viewHolder.checkBox.isChecked();
                if(isChecked){
                    if(checkedlist.size()>=4){
                        viewHolder.checkBox.setChecked(false);
                    }else{
                        if(!checkedlist.contains(position));
                        checkedlist.add(position);
                    }
                }else{
                    if(checkedlist.contains(position)){
                        int i = checkedlist.indexOf(position);
                        checkedlist.remove(i);
                    }
                }

            }
        });
        return v;
    }

    class ViewHolder{
        TextView tvName;
        CheckBox checkBox;
        ViewHolder(View view){
            tvName = view.findViewById(R.id.tvsongnameMERGE);
            checkBox = view.findViewById(R.id.checkBoxMERGE);
        }
    }

    public ArrayList<Integer> getArrayList(){
        return checkedlist;
    }
}
