package com.sama.musicplayer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;

public class MergingAdapter extends BaseAdapter {

    Context context;
    MergingAdapter mergingAdapter;
    ArrayList<File> selectedlist;

    public MergingAdapter(Context context, ArrayList<File> selectedlist){
        this.context = context;
        this.selectedlist = selectedlist;
    }

    @Override
    public int getCount() {
        return selectedlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder viewHolder;
        if (v==null){
            v = LayoutInflater.from(context).inflate(R.layout.merge_item, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) v.getTag();
        }

        if(selectedlist.contains(position)){
            viewHolder.checkBox.setChecked(true);
        }else{
            viewHolder.checkBox.setChecked(false);
        }
        viewHolder.tvName.setText(selectedlist.get(position).getName());
        viewHolder.checkBox.setChecked(true);
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked=viewHolder.checkBox.isChecked();
                if(!isChecked){
                    selectedlist.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(v.getContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    class ViewHolder{
        TextView tvName;
        CheckBox checkBox;
        ViewHolder(View view){
            tvName = view.findViewById(R.id.tvsongnameMERGE);
            checkBox = view.findViewById(R.id.checkBoxMERGE);
        }
    }
}
