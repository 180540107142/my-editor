package com.sama.musicplayer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;

public class TrimmingsAdapter extends BaseAdapter {

    Context context;
    ArrayList<File> trimmings;


    public TrimmingsAdapter(Context context, ArrayList<File> trimmings){
        this.context = context;
        this.trimmings = trimmings;
    }

    @Override
    public int getCount() {
        return trimmings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder viewHolder;
        if (v==null){
            v = LayoutInflater.from(context).inflate(R.layout.list_song, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) v.getTag();
        }
        viewHolder.tvName.setText(trimmings.get(position).getName());
        return v;
    }

    class ViewHolder{
        TextView tvName;
        ViewHolder(View view){
            tvName = view.findViewById(R.id.tvsongname);
        }

    }
}
