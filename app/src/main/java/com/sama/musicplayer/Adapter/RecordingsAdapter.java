package com.sama.musicplayer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sama.musicplayer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RecordingsAdapter extends RecyclerView.Adapter<RecordingsAdapter.RecordingsHolder> {

    Context context;
    ArrayList<File> fileList;
    OnItemClickListener onItemClickListener;

    public RecordingsAdapter(Context context, ArrayList<File> fileList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.fileList = fileList;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void Onclick(int position);
    }

    @NonNull
    @Override
    public RecordingsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecordingsHolder(LayoutInflater.from(context).inflate(R.layout.custom_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull RecordingsAdapter.RecordingsHolder holder, int position) {
        holder.tvRecordingname.setText(fileList.get(position).getName());
        holder.tvRecordingname.setSelected(true);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null){
                    onItemClickListener.Onclick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return fileList.size();
    }

    class RecordingsHolder extends RecyclerView.ViewHolder{

        public TextView tvRecordingname;
        public LinearLayout rcvrecordings;

        public RecordingsHolder(@NonNull View itemView) {
            super(itemView);
            tvRecordingname = itemView.findViewById(R.id.tvRecordingname);
            rcvrecordings = itemView.findViewById(R.id.rcvrecordings);
        }
    }

}
